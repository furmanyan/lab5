package lab5;

/**
 * Stuff class
 * Author: Furman Yans
 */
public class Stuff {

    private int count;

    public Stuff() {
        count=1;
        System.out.println("Stuff created");
    }

    public Stuff (int i){
        count=Math.abs(i==0 ? 1 : i)*3-2;
        System.out.println("Stuff created");
    }

    /* --- ---
    COMMent
    ---  --- */


    public void doStuff() {
        for(int i=0; i<count; i++) {
            System.out.println("Stuff");
        }
    }
    //comment
    public void doStuff(int i) {
        for(int k=0; k<i; k++) {
            System.out.println("Stuff");
        }
    }

    public void doStuff(String s) {
        for (char c : s.toCharArray()) {
            System.out.println("Stuff");
        }
    }
}