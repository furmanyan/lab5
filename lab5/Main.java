package lab5;

public class Main {

    /**
     * BigStuff class
     * Author: Furman Yans
     */
    private static class BigStuff extends Stuff {

        public BigStuff() {
            super(100);
        }

        public BigStuff(int i) {
            super((int)Math.pow(10, Math.abs(i==0 ? 1 : i)));
        }

        public void doBigStuff(int i) {
            for(int k=0; k<(int)Math.pow(5, i); i++) {
                System.out.println("BigStuff");
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Start of the program");
        System.out.println("Stuff creating...");
        Stuff s=new Stuff();
        s.doStuff(3);
        /**
         * first
         * second
         * third
         */
        //just comment
        s.doStuff("Hello");
        System.out.println("BigStuff creating...");
        BigStuff b=new BigStuff();
        b.doStuff(13);
        b=new BigStuff(5);
        b.doStuff();
        System.out.println("Stop of the program");
    }
    
}